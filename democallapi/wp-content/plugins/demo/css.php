<style>
.mt-3.alert.alert-success {
    word-break: break-word;
}
.mb-3 {
    display: flex;
    width: 100%;
}
form{
    width: 600px;
    margin: auto;
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: column;
    padding: 15px;
    border: 1px solid #333;
    margin-top: 30px;
}
label {
    width: 100px;
}
input {
    flex: 1;
    outline: none;
    border-radius: 4px;
    border: 1px solid #333;
}
</style>