<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * Database settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Database settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'demo2' );

/** Database username */
define( 'DB_USER', 'root' );

/** Database password */
define( 'DB_PASSWORD', '' );

/** Database hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '9r#Bu}oo.=oq@!,i=<|F}Y<LHNLL0%#at$we@MIp4KjrM+/t.Y!+ed)2^J:wFU_M' );
define( 'SECURE_AUTH_KEY',  'o[D-O SX451DRGkt| !ZS_G*RP*vBNtS%y%hcZS}icP~$fd!tp.J9o&^i}D@8lkZ' );
define( 'LOGGED_IN_KEY',    '-SuW1Ro|0^>?:|eR1.~1[)&_u1g.XF9%u$c^g3_0+US+[ b*;,mph=CuNXen=17i' );
define( 'NONCE_KEY',        '^2NFEoB|5AxM 0)W9;2l}$x`_6M5xv&qu/E&rgy yxoC`,02FQCF#)qT[qlLO:3,' );
define( 'AUTH_SALT',        'i,%~|B Cw:FAQDl*MtC$=m=48L Zie*SZ*?}Z2CI%6Gi34^.tfERFIHEY;94Yf/b' );
define( 'SECURE_AUTH_SALT', '2-1k4 1?G< z_Z`AG~YA[J0,Oxy[8PpOFgp7Caif`6:FHh>U.pIJM%/?4P5u_^(n' );
define( 'LOGGED_IN_SALT',   '5U2X.]gzNmSUjiYS9-I4afd_:, ,z{E-x;ph7DG0r=?jxuN]3_b^)=hCW,-pPN*y' );
define( 'NONCE_SALT',       ',uQO)w*,*T`O=ubo%`SU,6F0$gfzXyZ^90blxZMs*age?`tl1` s}JHLx|AbVlB7' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
