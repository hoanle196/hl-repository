const {gql} = require('apollo-server-express');
const typeDef = gql`
    type Book {
        id: ID
        name: String
        genre: String
    }
    #ROOT Type
    type Query {
        books: [Book]
    }
`
module.exports = typeDef