<?php 
    /**
 * Plugin Name:       call api wordpress
 * Plugin URI:        https://example.com/plugins/the-basics/
 * Description:       demo CRUD call api test
 * Version:           1.10.3
 * Requires at least: 5.2
 * Requires PHP:      7.2
 * Author:            hoanle2
 * Author URI:        https://author.example.com/
 * License:           GPL v2 or later
 * License URI:       https://www.gnu.org/licenses/gpl-2.0.html
 * Update URI:        https://example.com/my-plugin/
 * Text Domain:       my-basics-plugin
 * Domain Path:       /languages
 */
