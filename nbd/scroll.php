<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.1/jquery.min.js"></script>
    <link rel="stylesheet" href="css/style3.css">
    <title>scrollEffect</title>
</head>
<body>
    <section>
        <img src="assets/bg.jpg" alt="background" id="bg">
        <img src="assets/moon.png" alt="moon" id="moon">
        <img src="assets/mountain.png" alt="mountain" id="mountain">
        <img src="assets/road.png" alt="road" id="road">
        <h2 id="text">Moon Light</h2>
    </section>
    <ul>
        <p>1</p>
        <p>2</p>
        <p>3</p>
        <p>4</p>
        <p>5</p>
        <p>6</p>
        <li>
            <ul>
                <li>soasdf1</li>
                <li>soasdf2</li>
                <li>soasdf3</li>
            </ul>
        </li>
        <li>hoanle</li>
        <li>demo</li>
        <li>123</li>
    </ul>
    <script src="js/script3.js"></script>
</body>
</html>