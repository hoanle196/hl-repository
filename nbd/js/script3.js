jQuery(document).ready(function($){
    const bg = $("#bg");
    const moon = $("#moon");
    const mountain = $("#mountain");
    const road = $("#road");
    const text = $("#text");
    window.addEventListener("scroll", function(e) {
        const value = this.window.scrollY;
        bg.css('top',`${value * 0.25}px`)
        moon.css('left',`-${value * 2}px`)
        mountain.css('top',`-${value * 0.25}px`)
        road.css('top',`${value * 0.15}px`)
        text.css('top',`${value * 1}px`)
    })
})