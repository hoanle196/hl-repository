const app = angular.module('myApp',[]);
app.controller( 'myCtrl', ['$scope',
    '$location',
    '$http',
    '$timeout',
    '$interval',
    'callApi',
    function( $scope, 
        $location ,
        $http,
        $timeout,
        $interval,
        callApi ) {
    // $scope la doi tuong tham chieu toi file Html co controller myCtrl
    $scope.abc = {};
    $scope.abc.ab = {};
    $scope.abc.ab.a = $location.absUrl();
    console.log( $scope );
    // console.log( $location );
    // console.log( $http );
    // console.log( callApi );
    $scope.loginUrl = 'https://erp.cloodo.com/api/v1/auth/login';
    $scope.url = 'https://erp.cloodo.com/api/v1/invoice?offset=0&limit=100';
    $scope.data = {
        email: "herostorm1996@gmail.com",
        password: "lehoan123456"
    }
    $scope.token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczpcL1wvZXJwLmNsb29kby5jb21cL2FwaVwvdjFcL2F1dGhcL2xvZ2luIiwiaWF0IjoxNjY3MzYxMjQ1LCJleHAiOjE2OTg4OTcyNDUsIm5iZiI6MTY2NzM2MTI0NSwianRpIjoiVjl4V2lYczJtbWpUcVFoQSIsInN1YiI6NTQ1LCJwcnYiOiI4MThmNWM5OGFjZTIzNzUzMmQ5ZDQ5NDNmZDhlZmI1NDBiODU1YjQyIiwicmVtZW1iZXIiOjEsInR5cGUiOjF9.uA4OwN3W64T49yjBL75KPipQ9DizAdQ4ePN-QbKL7Rg';
    $scope.options = {
        headers: {
            // 'X-requested-Width': 'XMLHttpRequest',
            'Authorization': 'Bearer ' + $scope.token,
            'Content-Type': 'application/json'
        }
    }
    $timeout(function() {
        $scope.abc.ab.a = 'call API get total price: '+ $scope.url;
        $interval(()=> {
            $scope.time = new Date().toLocaleTimeString();
        },1000)
    },2000)
    
    $scope.dataGet = callApi._get($scope.url);
    // console.log( $scope.dataGet );

    $scope.dataPost = callApi._post( $scope.loginUrl, $scope.data );
    // console.log( $scope.dataPost );

    // datanew ={};
    const promise = new Promise((success, error) => {
        url = 'https://erp.cloodo.com/api/v1/invoice?offset=0&limit=100';
        const dataGet = callApi._get(url);
        if ( dataGet ) {
            // angular.copy(dataGet, datanew)
            success(dataGet);     
        } else {
            error('call api loi 1');
        }
    }); 
    // console.log(promise);
    // const promise2 = new Promise((success, error) => {
    //     urlLog = 'https://erp.cloodo.com/api/v1/auth/login';
    //     data1 = {
    //         email: "herostorm1996@gmail.com",
    //         password: "lehoan123456"
    //     }
    //     const data = callApi._post(urlLog, data1);
    //     if ( data !== null ) {
    //         success(data);     
    //     } else {
    //         error('call api loi 2');
    //     }
    // }); 
    const test = async () => {
        url = 'https://erp.cloodo.com/api/v1/invoice?offset=0&limit=100';
        urlLog = 'https://erp.cloodo.com/api/v1/auth/login';
        user = {
            email: "admin@example.com",
            password: "123456"
        }
        try {
            $scope.dataGet = await callApi._get(url);
            console.log(1, $scope.dataGet);
            console.log(1, $scope.dataGet.data);
            $scope.data2 = await callApi._post(urlLog, user);
            console.log(2, $scope.data2);
        } catch (error) {
            console.log(error);
        }
    }
    test();
    jQuery( document ).ready( function() {
        const arr = jQuery('.nbd-img img');
        jQuery.each( arr, function( index, value ) {
            value.addEventListener( 'click', function(e) {
                jQuery( '.nbd-content img ').attr( 'src', jQuery(this).attr('src'));
                var a = jQuery( '.nbd-content img ').attr( 'src' );
                console.log($(this).attr('src'));
            })
        });
        jQuery( document ).on( 'click', '.nbd-pre-js', function() {
    
        });
    });
}]);
app.service('callApi', ['$http', function($http) {
    this._get = (urlGet) => {
        return $http( {
            method: 'GET',
            url: urlGet,
            headers: {
                // 'X-requested-Width': 'XMLHttpRequest',
                'Authorization': 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczpcL1wvZXJwLmNsb29kby5jb21cL2FwaVwvdjFcL2F1dGhcL2xvZ2luIiwiaWF0IjoxNjY4NTg1MzE1LCJleHAiOjE3MDAxMjEzMTUsIm5iZiI6MTY2ODU4NTMxNSwianRpIjoiUUdySzk4ck1mMURTMkpDUCIsInN1YiI6MiwicHJ2IjoiODE4ZjVjOThhY2UyMzc1MzJkOWQ0OTQzZmQ4ZWZiNTQwYjg1NWI0MiIsInJlbWVtYmVyIjoxLCJ0eXBlIjoxfQ.9km9PPziW5b3h5iAwpYL6xp1_JlgBVEtmlS0IBKP-9k',
                'Content-Type': 'application/json'
            }
        } ).then( function( res ) {
            return res.data;
        },function(error) {
            return error;
        })
    };
    this._post = (urlPost, formData) => {
        return $http( {
            method: 'POST',
            url: urlPost,
            data: formData
            // headers: {
            //     // 'X-requested-Width': 'XMLHttpRequest',
            //     'Authorization': 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczpcL1wvZXJwLmNsb29kby5jb21cL2FwaVwvdjFcL2F1dGhcL2xvZ2luIiwiaWF0IjoxNjY4NTg1MzE1LCJleHAiOjE3MDAxMjEzMTUsIm5iZiI6MTY2ODU4NTMxNSwianRpIjoiUUdySzk4ck1mMURTMkpDUCIsInN1YiI6MiwicHJ2IjoiODE4ZjVjOThhY2UyMzc1MzJkOWQ0OTQzZmQ4ZWZiNTQwYjg1NWI0MiIsInJlbWVtYmVyIjoxLCJ0eXBlIjoxfQ.9km9PPziW5b3h5iAwpYL6xp1_JlgBVEtmlS0IBKP-9k',
            //     'Content-Type': 'application/json'
            // }
        } ).then( function( res ) {
            return res.data;
        },function(error) {
            return error;
        })
    }
}]);
app.filter('myFilter', ['callApi', function(callApi) {
    return function(url) {
        // datanew ={};
        // const promise = new Promise((success, error) => {
        //     const dataGet = callApi._get(url);
        //     if ( dataGet ) {
        //         angular.copy(dataGet, datanew)
        //         success(dataGet);     
        //     } else {
        //         error('call api loi 1');
        //     }
        // }); 
        // console.log(promise);
        // const promise2 = new Promise((success, error) => {
        //     urlLog = 'https://erp.cloodo.com/api/v1/auth/login';
        //     data1 = {
        //         email: "herostorm1996@gmail.com",
        //         password: "lehoan123456"
        //     }
        //     const data = callApi._post(urlLog, data1);
        //     if ( data !== null ) {
        //         success(data);     
        //     } else {
        //         error('call api loi 2');
        //     }
        // }); 
        // const test = async () => {
        //     try {
        //         const data1 = await promise;
        //         const a = await data1;
        //         console.log(1, a.data);
        //         const data2 = await promise2;
        //         const b = await data2;
        //         console.log(2, b.message);
        //     } catch (error) {
        //         console.log(error);
        //     }
        // }
        // test();

        // promise.then( datat => {
        //     console.log(1,datat);
        //         return promise2
        //     }).then((data) => {
        //         console.log(2,data);
        //     })
        //     .catch( error => {
        //         console.log( error );
        //     })

        // const data = callApi._post(url, data1);
        // const dataString = JSON.stringify(data);
        // console.log(typeof dataString);
        // setTimeout(function() {
        //     datanew = data.message;
        // },3000);
        return 'datanew';
    }
}]);
