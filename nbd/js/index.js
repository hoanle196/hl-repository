console.log("hello");
document.querySelector("#canvas");
const canvas = new fabric.Canvas("canvas", {
    width:800,
    height:500,
    backgroundColor:'#444'
});
console.log(canvas);
var obj =  new fabric.Circle({
    width:100,
    height:100,
    radius: 20, 
    fill: 'green',
    left: 100,
    top: 100
});
var triangle = new fabric.Triangle({
    width: 100, height: 100, fill: 'blue', left: 200, top: 100
});
const img = document.querySelector("#my-image");
var imgCanvas = new fabric.Image(
    img, {
        left: 100,
        top: 250,
        // angle: 30,
        // opacity: 0.85,
        // width:500,
        // height:500,
        // cornerSize:200,
        hoverCursor:'pointer',
    }
);
fabric.Image.fromURL("https://bold.vn/wp-content/uploads/2019/05/bold-academy-5.jpg", function(img){
    img.set({
        // width: 200,
        // height: 200,
        top: 100,
        left: 500,
    });
    img.scaleToWidth(300);
    img.scaleToHeight(300);
    console.log(canvas.width);
    console.log(canvas.height);
    console.log(img);
    canvas.add(img);
})
var svg = `<svg version='1.1' xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' x='0px' y='0px' width='57.08px' height='74.223px' viewBox='0 0 57.08 74.223' enable-background='new 0 0 57.08 74.223' xml:space='preserve'>
<style>
    #my-image {
        background-color: red;
    }
</style>
<path d='M0,0l27.281,38.375L55.063,1.5L0,0z M15.156,8.063l26.281,1.531L27.281,26.25L15.156,8.063z'/>
<path d='M14.653,8.058L27.28,25.23l2.021,1.01L40.917,8.563L14.653,8.058z'/>
<path d='M6.572,43.413l0.505,7.576l50.003,0.505l-0.505-7.071L6.572,43.413z'/>
<path d='M7.077,54.02l0.505,7.576l49.498,1.01l-0.505-7.071L7.077,54.02z'/>
<path d='M7.582,65.131l0.505,8.586l48.993,0.505v-6.566L7.582,65.131z'/>
</svg>`;


// parser = new DOMParser();
// xmlDoc = parser.parseFromString(text,"text/xml");
fabric.loadSVGFromString(svg, function(ob, op){
    console.log('element', ob);
    console.log('option', op);
    var obj1 = fabric.util.groupSVGElements(ob, op);
    obj1.scaleToWidth(500);
    obj1.scaleToHeight(500);
    console.log('obj',obj1);
    canvas.add(obj1);
});
// canvas.on('object:moving', function (event) {
//     alert("Oke");
//     recCanvas(canvas) // fire this if finished
// });
canvas.on("object:modified", function (e) {
    alert("object modified");
});
triangle.set('top', 50)
triangle.set('left', 50)
// triangle.setLeft(50);
// triangle.setTop(50);
triangle.setCoords();
canvas.renderAll();
// triangle.trigger('object:moving', function(){
//     alert("PLs")
// });
canvas.add(obj, triangle, imgCanvas);
// canvas.trigger('object:moving', function (event) {
//     alert("Oke");
//     console.log(event);
//     recCanvas(canvas) // fire this if finished
// });