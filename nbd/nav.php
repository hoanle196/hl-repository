<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- <link rel="stylesheet" href="css/style1.css"> -->
    <link rel="stylesheet" href="css/style2.css">
    <title>navication</title>
</head>
<body>
    <div class="navication">
        <ul>
            <li class="list active">
                <a>
                    <span class="icon"><ion-icon name="home-outline"></ion-icon></span>
                    <span class="text">Home</span>
                </a>
            </li>
            <li class="list">
                <a>
                    <span class="icon"><ion-icon name="person-outline"></ion-icon></span>
                    <span class="text">Profile</span>
                </a>
            </li>
            <li class="list">
                <a>
                    <span class="icon"><ion-icon name="camera-outline"></ion-icon></span>
                    <span class="text">Photos</span>
                </a>
            </li>
            <li class="list">
                <a>
                    <span class="icon"><ion-icon name="chatbubble-outline"></ion-icon></span>
                    <span class="text">Message</span>
                </a>
            </li>
            <li class="list">
                <a>
                    <span class="icon"><ion-icon name="settings-outline"></ion-icon></span>
                    <span class="text">Setting</span>
                </a>
            </li>
            <div class="indication"></div>
        </ul>
    </div>
    <!-- iconic -->
    <script type="module" src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.esm.js"></script>
    <script nomodule src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.js"></script>
    <script src="js/script2.js"></script>
</body>
</html>