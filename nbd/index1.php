<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.1/jquery.min.js"></script>
    <link rel="stylesheet" href="css/style.css">
    <title>demo</title>
</head>
<body>
    <div  ng-app="myApp" ng-controller="myCtrl" id="container">
        <div class="side_bar">
            <div class="nbd-list-img" >
                <h1>{{time}}</h1>
                <h1>{{ 'https://erp.cloodo.com/api/v1/invoice?offset=0&limit=100' | myFilter }}</h1>
                <h1>{{data2.message || 'Loading...'}}</h1>
                <?php
                    $dir ='img';
                    $files = scandir($dir, 0);
                for ($i = 2; $i < count($files); $i++) { ?>
                        <div class="nbd-img">
                            <img src='img/<?php echo $files[$i]?>' alt='anh-dep'> 
                        </div>
                <?php } ?>
                <h3>{{abc.ab.a}}</h3>
                <div ng-repeat="item in dataGet.data" style="border:solid 1px red; margin: 10px">
                {{item.total}}
            </div>
                <!-- <h3>{{test}}</h3> -->
            </div>
            <div class="nbd-button">
                <button class="btn btn-warning nbd-pre-js">Prerious</button>
                <button class="btn btn-success nbd-next-js">Next</button>
            </div>
        </div>
        <div class="content">
            <div class="nbd-content">
                <img src="img/1.jpg" alt="">
            </div>
        </div>
    </div>
</body>
<script src="js/script.js"></script>
</html>
