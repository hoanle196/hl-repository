<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/background.css">
    <title>Background-Effect</title>
</head>
<body>
    <div class="container">
        <div class="bubbles">
            <span style="--i:19;"></span>
            <span style="--i:12;"></span>
            <span style="--i:11;"></span>
            <span style="--i:80;"></span>
            <span style="--i:10;"></span>
            <span style="--i:99;"></span>
            <span class="yellow" style="--i:22;"></span>
            <span class="yellow" style="--i:41;"></span>
            <span class="yellow" style="--i:4;"></span>
            <span style="--i:24;"></span>
            <span style="--i:46;"></span>
            <span style="--i:26;"></span>
            <span style="--i:33;"></span>
            <span style="--i:44;"></span>
            <span style="--i:89;"></span>
            <span style="--i:25;"></span>
            <span style="--i:69;"></span>
            <span style="--i:42;"></span>
            <span class="yellow" style="--i:66;"></span>
            <span class="yellow" style="--i:44;"></span>
            <span class="yellow" style="--i:22;"></span>
            <span class="yellow" style="--i:11;"></span>
            <span class="yellow" style="--i:88;"></span>
            <span style="--i:19;"></span>
            <span style="--i:52;"></span>
            <span style="--i:16;"></span>
            <span style="--i:53;"></span>
            <span style="--i:18;"></span>
            <span style="--i:20;"></span>
            <span style="--i:21;"></span>
            <span style="--i:23;"></span>
            <span style="--i:18;"></span>
            <span class="yellow" style="--i:95;"></span>
            <span class="yellow" style="--i:41;"></span>
            <span class="yellow" style="--i:62;"></span>
            <span class="yellow" style="--i:76;"></span>
            <span class="yellow" style="--i:87;"></span>
            <span class="yellow" style="--i:98;"></span>
            <span style="--i:27;"></span>
            <span style="--i:49;"></span>
            <span style="--i:53;"></span>
            <span style="--i:18;"></span>
            <span style="--i:20;"></span>
            <span style="--i:21;"></span>
            <span style="--i:23;"></span>
            <span class="yellow" style="--i:20;"></span>
            <span class="yellow" style="--i:78;"></span>
            <span class="yellow" style="--i:71;"></span>
        </div>
    </div>
</body>
</html>