<?php
class Home extends Hooks{
    public function index() {
        $dataGet = $this->model('SinhvienModel');
        echo $dataGet->GetSV();
    }
    public function Show($id = null) {
        $dataGet = $this->model('SinhvienModel');
        $res = $dataGet->GetSV($id);
        $this->title = 'Sv-View';
        $this->content = $this->view('aodep', ['data'=>$res]);
        require_once("./mvc/views/layout/main.php");
    }
    
}