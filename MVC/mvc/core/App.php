<?php
class App {
    public $controller="Home";
    protected $action="index";
    protected $params=[];
    public function __construct() {
        $arrUrl = $this->processUrl();
        // process controller
        if (isset($arrUrl) && file_exists("./mvc/controllers/{$arrUrl[0]}.php")){
            $this->controller = ucfirst($arrUrl[0]);
            unset($arrUrl[0]);
        }
        require_once("./mvc/controllers/$this->controller.php");
        // process action
        if(isset($arrUrl[1]) && method_exists($this->controller, $arrUrl[1])) {
            $this->action = $arrUrl[1];
            unset($arrUrl[1]);
        }
        // process params
        $this->params = $arrUrl ? array_values($arrUrl) : [];
            $controller = $this->controller;
            $action = $this->action;
            $params = $this->params;
            $obj = new $this->controller();
        // + Gọi phương thức để thực thi chức năng
        if (!method_exists($obj, $action)) {
            die("Class $controller ko tồn tại phương thức $action");
        }
        $obj->$action($params);
        // call_user_func_array([$this->controller, $this->action], $this->params);
    }
    protected function processUrl() {
        if (isset($_GET['url'])) return explode('/', filter_var(trim($_GET['url'], '/')));
    }
}