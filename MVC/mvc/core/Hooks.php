<?php 
class Hooks {
    public $title = 'trang chu';
    public $content;
    public function model($model){
        require_once("./mvc/models/$model.php");
        return new $model;
    }
    public function view($view, $args = []){
        ob_start();
        require_once("./mvc/views/$view.php");
        $content = ob_get_clean();
        return $content;
    }
}