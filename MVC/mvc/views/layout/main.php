
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../public/css/styles.css">
    <title><?= $this->title ?></title>
</head>
<body>
        <header>Đây là header</header>
        <main>
            <?php echo $this->content ?>
        </main>
        <footer>Đây là footer</footer>
    </body>
</html>