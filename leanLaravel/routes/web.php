<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\PostsController;
use App\Http\Controllers\FoodsController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/                              

Route::get('/', [HomeController::class, 'index'])->name('index');
Route::get('/product', [ProductController::class, 'index'])->name('product');
Route::get('/post', [PostsController::class, 'index'])->name('post');
Route::get('/register', [ProductController::class, 'index'])->name('register');
Route::get('/food', [FoodsController::class, 'index'])->name('food');
Route::prefix('food')->group(function () {
    Route::get('/create', [FoodsController::class, 'create'])->name('create');
    Route::post('/create', [FoodsController::class, 'store'])->name('store');
    Route::get('/edit/{id}', [FoodsController::class, 'edit'])->name('edit');
    Route::put('/edit/{id}', [FoodsController::class, 'update'])->name('update');
    Route::get('/destroy/{id}', [FoodsController::class, 'destroy'])->name('destroy');
    
});