<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PostsController extends Controller
{
    //
    public function index() {
        //query builders
        // $posts = DB::select("SELECT * FROM posts WHERE id = :id",[
        //     "id" => 1,
        // ]);
        // c2 (fillter table posts width id = 1) auto create query
        $posts = DB::table('posts') //loc tu bang post
            ->find(1); // fillter theo id
            // ->count();// cout(*)
            // ->max('id');
            // ->min('id');
            // ->sum('id');
            // ->avg('id');
        //insert
            // ->insert([
            //     'title' => 'hoanle',
            //     'description' => '16101996',
            // ]);
            //::create
        //update
            // ->where('id', 1)
            // ->update([
            //     'title' => 'noi voi toi tuoi 31',
            //     'description' => 'se co gang nhieu hon',
            // ]);
        //delete
            // ->where('id', 2)
            // ->delete();
                    // ->select('title') // voi cac field ?? all field
                    // ->where('id','=', 1) // voi dieu kien ??
                    // ->first() // voi dieu kien ??
                    // ->latest() // voi dieu kien ??
                    // ->orderBy('id', 'DESC') // voi dieu kien DESC - giam ASC- tang
                    // ->whereNotNull('abc') // voi dieu kien ??
                    // ->whereBetween('id', [1,3]) // voi dieu kien ?? id nam trong khoang 1 -3
                    // ->get();
        // dd($posts);
        return view('posts.index');
    }
}
