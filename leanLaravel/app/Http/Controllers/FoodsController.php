<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Food;

class FoodsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index( ) //<-Request $request
    {
        // if ($request->session()->has('key')) {
            // dd(session('status')['message']) ;
        //     $request->session()->forget('key');
        // }
        $foods = Food::all();
        $item = 5;
        $foods = Food::paginate($item);
        $pageSum = $foods->lastPage();
        $pageNum = $foods->currentPage();
        $around = 2;
        $next = $pageNum + $around;
        if ($next > $pageSum) {
                $next = $pageSum;
        }
        $pre = $pageNum - $around;
        if ($pre <= 1) $pre = 1;
        $ofset = (request()->input('page', 1) -1 ) * $item;
        // $foods = Food::where('name','hoanle')
        // ->update([
        //     'name'=>'123',
        //     'description'=>' viet nam',
        // ]);
        // ->get();


        return view('foods.index',[
            "foods" => $foods ?? '',
            "ofset" => $ofset ?? '',
            "next"=> $next ?? '',
            "pre"=> $pre ?? '',
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('foods.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Food $foods)
    {
        // $result = Food::create($request->all()); c1
        $foods->name = $request->input('name'); //c2
        $foods->count = $request->input('count'); 
        $foods->description = $request->input('description');
        $foods->save();
        session(['status' => [
            'message' => 'add food successful',
            'class' => 'success',
        ]]);
        // $foods::where('id', 4)->update([
        //     "name" => $request->input('name'),
        //     "count" => $request->input('count'),
        //     'description' => $request->input('description'),
        // ]);
        // [
        //     'message' => 'show foods',
        //     'class' => 'success',
        // ]
        return redirect('food');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $arr = Food::find($id);
        return view('foods.edit', ['id' => $id,'food' => $arr]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Food $foods, $id)
    {
        //c1
        // $query = new Food();
        // $query->where('id', $id)
        //     ->update([
        //         "name" => $request->input('name'),
        //         "count" => $request->input('count'),
        //         "description" => $request->input('description'),
        //     ]);
        //c2
        $food = $foods->find($id);
        $food->name = $request->input('name');
        $food->count = $request->input('count');
        $food->description = $request->input('description');
        $food->save();
        session(['status' => [
            'message' => 'update successfuly',
            'class' => 'success',
        ]]);
        return redirect('food');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Food::find($id)->delete();
        session(['status' => [
            'message' => 'delete successfuly',
            'class' => 'success',
        ]]);
        return redirect('food');
    }
}
