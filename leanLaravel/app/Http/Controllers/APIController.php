<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Http\Resources\Product as ProductResource;
use Illuminate\Support\Facades\Validator;

class ProductsController extends Controller
{
    public function index()
    {
        // get all
        $products = Product::all();
        // return $products;
        $arr = [
        'status' => true,
        'message' => "Danh sách sản phẩm",
        'data'=>ProductResource::collection($products)
        ];
        return response()->json($arr, 200);
    }
    public function create()
    {
        //
    }
    public function store(Request $request)
    {
        // post insert into
        $inputField = $request->all();
        // return $inputField;
        // $validator = Validator::make($inputField, [
        //     "name" => 'require',
        //     "price" => 'require',
        // ]);
        // if ( $validator->fails() ) {
        //     $data = [
        //         "Status" => '400-499',
        //         "Message" => 'fails !',
        //         "Data" => $validator->errors(),
        //     ];
        //     return response()->json($data, 400);
        // }
        $product = Product::create($inputField);
        $data = [
            "Status" => '200',
            "Message" => 'add new successfully !',
            "Data" =>new ProductResource($product),
        ];
        return response()->json($data, 200);


    }
    public function show($id)
    {
        // get details
        $product = Product::find($id);
        // $product->find($id);
        if (is_null($product)) {
            $arr = [
                'success' => false,
                'message' => 'Không có sản phẩm này',
            ];
            return response()->json($arr, 200);
        }
        $arr = [
            'status' => true,
            'message' => "Chi tiết sản phẩm ",
            'data'=> new ProductResource($product)
        ];
        return response()->json($arr, 201);
    }
    public function edit($id)
    {
        //
    }
    public function update(Request $request, Product $product)
    {
        // put 
        $inputField = $request->all();
        //validate
        // ...
        $product->name = $inputField['name'];
        $product->price = $inputField['price'];
        $product->save();
        $data = [
            "Status" => 'true',
            "Message" => 'update successfully !',
            "Data" => $product,
        ];
        return response()->json($data, 200);

    }
    public function destroy(Product $product)
    {
        // delete
        $product->delete();
        $arr = [
            'status' => true,
            'message' =>'delete successfully !',
        ];
        return response()->json($arr, 200);
    }
}
