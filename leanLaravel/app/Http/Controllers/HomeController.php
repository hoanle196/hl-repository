<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    //
    public function index() {
        $value = [
            "name"=> 'honale',
            "birthday"=> '16/10/1996',
            "gender"=> 'nam',
        ];
        // 3 cach truyen tham so cho view
        // return view('home')->with('value',123); // c1
        // return view('home', [ 
        //     "value" => 'hoanleaa'
        // ]);//c2
        return redirect('food');
        return view('index', compact('value')); //c3
    }
    
    
}
