<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Product extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        return [
            'id' => $this->id,
            'ten_sp' => $this->name,
            'gia' => $this->price,
            'ngay tao' => $this->created_at->format('d/m/Y'),
            'ngay cap nhat' => $this->updated_at->format('d/m/Y'),
        ];
    }
}
