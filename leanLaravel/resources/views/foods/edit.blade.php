@extends('layouts.main')
@section ('content')
<div class="container">
    <h1>Edit food</h1>
        <form class="frm" action="{{ route('update', $id) }}" method="POST">
        @csrf
        @method('PUT')
        <div class="mb-3">
            <label for="name"  class="form-label">name</label>
            <input type="text" name="name" class="form-control" id="name" value="{{$food->name}}" aria-describedby="emailHelp">
        </div>
        <div class="mb-3">
            <label for="count"  class="form-label">count</label>
            <input 
                type="number" 
                name="count" 
                min="0" 
                max="1000" 
                class="form-control" 
                id="count"
                value="{{$food->count}}" 
                aria-describedby="emailHelp">
        </div>
        <div class="mb-3">
            <label for="description">Description</label>
            <textarea class="form-control" id="description" name="description" rows="4" cols="50">{{$food->description}}</textarea>
        </div>
        <button type="submit" class="btn btn-primary">save</button>
        <a href="{{ route('food') }}" type="submit" class="btn btn-secondary">back</a>
    </form>
</div>
@endsection