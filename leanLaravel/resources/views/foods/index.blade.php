@extends('layouts.main')
@section ('content')
@include('layouts.message')
<div class="container">
    <a href="{{route('create')}}"><button type="button" class="btn btn-primary">add new Food</button></a>
    <table class="table">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">name</th>
                <th scope="col">count</th>
                <th scope="col">description</th>
                <th scope="col">action</th>
            </tr>
        </thead>
        <tbody>
        
            @foreach ( $foods as $index=>$food ) 
                <tr>
                    <th scope="row">{{++$ofset}}</th>
                    <td><a href="">{{$food->name}}</a></td>
                    <td>{{$food->count}}</td>
                    <td>{{$food->description}}</td>
                    <td>
                        <a class="btn btn-info" href="{{ route('edit', $food->id) }}">edit</a>
                        <button type="button" class="btn btn-danger" data-url ="{{ route('destroy', $food->id) }}" data-bs-toggle="modal" data-bs-target="#exampleModal">delete</button>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    {{ $foods->links('layouts.customPaginate', compact('ofset', 'next', 'pre'))}}
    {{-- {{ $foods->links()}} --}}
</div>
@endsection