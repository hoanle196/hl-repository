<?php 
class Custom_API {
    private $method;
    public function __construct($method) {
        $this->method = $method;
    }
    public function process_request() {
        switch($this->method) {
            case 'GET':
                $response = $this->get_all();
                break;
            case 'POST':
                $response = $this->post_data();
                break;
        }
        print $response;
    }
    public function get_all() {
        $dir = str_replace("\\",'/', NBDESIGNER_PLUGIN_DIR.'img');
        $urlSite = str_replace("\\",'/',NBDESIGNER_PLUGIN_URL.'img/');
        $files = scandir($dir, 0);
        $arrCustom = [];
        for( $i = 2; $i < count($files); $i++ ) {
            $arrCustom[] = $urlSite.$files[$i];
        }
        return '{
            "records":'.json_encode(array_unique($arrCustom)).',
            "response":{
                "status_code":200,
                "message":"successfully"
            }
        }';
    }
    function post_data() {
        return '{
            "name": "hoanle",
            "birth_day": 16-10-1996,
            "contry": "viet nam"
            "details": {
                "tall":"1.75"
                "weight":"60"
                "job":"DEV"
            }
        }';
    }
}