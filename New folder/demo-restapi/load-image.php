<?php
// if (!defined('ABSPATH')) {
//     exit; // Exit if accessed directly
// }
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

$uri = trim(parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH));
$uri2 = explode( '/', $uri );

if ($uri2[2] === 'image-api-v1') {
    $method = $_SERVER["REQUEST_METHOD"];
    $controller = new Custom_API($method);
    $controller->process_request();
    exit;
}

