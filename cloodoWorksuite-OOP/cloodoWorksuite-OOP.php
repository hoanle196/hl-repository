<?php 
// use Clws_add_menu;
// use PHP_CodeSniffer\Standards\Squiz\Sniffs\Strings\EchoedStringsSniff;
// use PHP_CodeSniffer\Standards\Squiz\Tests\Strings\EchoedStringsUnitTest;
/**
 * Plugin Name:       demo Worksuite-OBJ
 * Plugin URI:        https://worksuite.cloodo.com/
 * Description:       Project management, trusted badge review
 * Version:           2.0.0
 * Requires at least: 5.2
 * Requires PHP:      7.2
 * Author:            Cloodo
 * Author URI:        https://cloodo.com/
 * License:           GPL v2 or later
 * License URI:       https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain:       cloodo-worksuite
 * Domain Path:       /languages
 **/

if ( !function_exists( 'add_action' ) ) {
	echo 'Hi there!  I\'m just a plugin, not much I can do when called directly.';
	exit;
}
add_action('init', function() {
	if ( is_user_logged_in() ) {
		define( 'CLWS_VERSION', '2.0.0' );
		define( 'CLWS_IFRAME_URL', 'https://worksuite.cloodo.com/' );
		//define( 'CLWS_IFRAME_URL', 'http://localhost:8222/login-app/' );
		//API user
		define( 'CLWS_API_LOGIN_URL', 'https://erp.cloodo.com/api/v1/auth/login' );
		define( 'CLWS_API_CREATE_URL', 'https://erp.cloodo.com/api/v1/create-user' );
		define( 'CLWS_API_USER_DATA_URL', 'https://erp.cloodo.com/api/v1/auth/me' );
		//API client
		define( 'CLWS_API_POST_CLIENT_URL', 'https://erp.cloodo.com/api/v1/client' );
		define( 'CLWS_API_GET_CLIENT_URL', 'https://erp.cloodo.com/api/v1/client/?fields=id,name,email,mobile,status,created_at,client_details{company_name,website,address,office_phone,city,state,country_id,postal_code,skype,linkedin,twitter,facebook,gst_number,shipping_address,note,email_notifications,category_id,sub_category_id,image}&offset=0' );
		define( 'CLWS_API_GET_ALL_CLIENT_URL', 'https://erp.cloodo.com/api/v1/client/?fields=id,name,email,mobile,status,created_at,client_details{company_name,website,address,office_phone,city,state,country_id,postal_code,skype,linkedin,twitter,facebook,gst_number,shipping_address,note,email_notifications,category_id,sub_category_id,image}&offset=0&limit=' );
		//API product
		define( 'CLWS_API_POST_PRODUCT_URL', 'https://erp.cloodo.com/api/v1/product' );
		define( 'CLWS_API_GET_PRODUCT_URL', 'https://erp.cloodo.com/api/v1/product/?fields=id,name,price,description,taxes,allow_purchase,category,hsn_sac_code&offset=0' );
		define( 'CLWS_API_GET_ALL_PRODUCT_URL', 'https://erp.cloodo.com/api/v1/product/?fields=id,name,price,description,taxes,allow_purchase,category,hsn_sac_code&offset=0&limit=' );
		//API invoices
		define( 'CLWS_API_GET_INVOICES', 'https://erp.cloodo.com/api/v1/invoice?offset=0' );
		// define( 'CLWS_API_GET_ALL_INVOICES', 'https://erp.cloodo.com/api/v1/invoice?offset=0&limit=' );
		define( 'CLWS_API_GET_ALL_INVOICES', 'https://erp.cloodo.com/api/v1/invoices?offset=0&limit=' );
		define( 'CLWS_API_POST_INVOICES', 'https://erp.cloodo.com/api/v1/invoice' );
		//plugin path
		define( 'CLWS_PLUGIN_URL', plugin_dir_url( __FILE__ ) );
		define( 'CLWS_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );

		require_once( str_replace( '\\', '/', CLWS_PLUGIN_DIR.'includes/Clws_auto_load.php' ) ); 

	    ///////////////////// add menu page //////////////////
		new Clws_add_menu;
		// $ten = "hoanle";
		// function test(&$a) {
		// 	$a = "hoanle 123";
		// 	$b = $a;
		// 	echo $b;
		// }
		// test($ten);
		// echo "\n";
		// echo $ten;
		// exit;

		// add_action('add_meta_boxes', function() {
		// 	add_meta_box(
		// 		'test',
		// 		'hoanle demo meta box',
		// 		'demo_meta_box',
		// 		'post',
		// 		'side',
		// 		'hight',
		// 		array('thamso 1, tham so 2, tham so 3')
		// 	);
		// });
		// function demo_meta_box($post, $metabox){ 
		// 	echo "Nội dung của metabox chữ ký"; 
		// }

		// function add_metabox_chu_ky(){ 
		// 	add_meta_box('chu_ky', 'Chữ ký', 'inner_metabox', 'post', 'side', 'high', array('tham so 1', 'tham so 2', 'tham so 3')); 
		// }
		// function inner_metabox($post, $metabox){
		// 	// Input hidden bảo mật
		// 	wp_nonce_field(basename(__FILE__), "chu_ky_post-nonce");
		 
		// 	$noi_dung_chu_ky = get_post_meta( $post->id,'chu_ky_post', true);
		// 	if( !isset( $noi_dung_chu_ky ) ) {
		// 		register_post_meta( 'post', $field, array('type' => 'string'));
		// 		$noi_dung_chu_ky = ' ';
		// 	}
		 
		// 	?/>
		// 	<p>Nội dung chữ ký:</p>
		// 	<textarea id="w3review" name="chu_ky_post" style="width: 100%; min-height: 150px;" value="<?php echo esc_html($noi_dung_chu_ky)?/>"></textarea>
		// 	<?php 
		// }
		// add_action('add_meta_boxes', 'add_metabox_chu_ky');
		 
		// function save_metabox_chu_ky_data($post_id, $post, $update) {
		// 	// Đây chính là input hidden Security mà ta đã tạo ở hàm show_metabox_contain
		// 	if (!isset($_POST["chu_ky_post"]) || !wp_verify_nonce($_POST["chu_ky_post-nonce"], basename(__FILE__))){
		// 		return $post_id;
		// 	}
		// 	// Kiểm tra quyền
		// 	if(!current_user_can("edit_post", $post_id)){
		// 		return $post_id;
		// 	}
		// 	// Kiểm tra có phải auto save không
		// 	if(defined("DOING_AUTOSAVE") && DOING_AUTOSAVE){
		// 		return $post_id;
		// 	}
		// 	if('post' != $post->post_type){
		// 		return $post_id;
		// 	}
				  
		// 	// Lấy thông tin từ field
		// 	$noi_dung_chu_ky = (isset($_POST["chu_ky_post"])) ? $_POST["chu_ky_post"] : '';
			  
		// 	// Cập nhật thông tin, hàm này sẽ tạo mới nếu như trong db chưa tồn tại
		// 	update_post_meta($post_id, "chu_ky_post", $noi_dung_chu_ky);
		// }
		  
		// add_action('save_post', 'save_metabox_chu_ky_data', 10, 3);
		// $value = get_option('cloodo_tokenn','');
		// // echo $value;

		// function thachpham_change_copyright( $output ) {
		// 	$output = 'Design by WordPress';
		// 	return $output;
		// }
		// add_filter( 'thachpham_copyright', 'thachpham_change_copyright' );
		// add_filter('thachpham_copyright',function($output, $flag) {
		// 	if($flag == 1) {
		// 		$a = 'hoan_le hello world, '.$output;
		// 	} else {
		// 		$a = 'nhun_at hello world, '.$output;
		// 	}
		// 	echo $a;
		// },20,2);
		// $copyright = 'Design by ThachPham';
		// $flag = 2;
        // echo apply_filters( 'thachpham_copyright', $copyright, $flag );
		// exit;
		// $id = get_the_ID();
		// global $product;
		// $post_id = $product->get_id();

		// $regular_price = get_post_meta( 121,'_nbes_settings', true);
		// $custom = get_post_custom(121);
		// $a = unserialize($regular_price);
		// print_r($a);
		// print_r($regular_price);
		// print_r($custom);
		// exit;
		// $sale_price = get_post_meta( $post_id, '_sale_price', true);
		// die('ket qua: '.$regular_price);
		// exit;
		// global $wpdb;
		// echo '<pre>';
		// print_r($wpdb);
		// echo '</pre>';
		// add_filter('action_custom',function($data,$prop = null) {
		// 	echo "$data $prop";
		// },10,2);
		// apply_filters('action_custom','hoanle123'); 
		// add_action('admin_menu', [$Clws_add_menu,'Register']);
    }
});
// add_action( 'teamplate_redirect','demo_redirect' ) ;
// function demo_redirect($a) {
// 	echo $a;
// 		if( $a == 'OK' ) {
// 			echo $a;
// 			wp_redirect( 'https://developer.wordpress.org/reference/hooks/template_redirect/' );
// 		}
// };
// echo has_action('teamplate_redirect','demo_redirect');
// exit;
// add_filter( 'hoanle_filter', function($output) {
// 	// $output = 'hehe';
// 	return $output;
// });
// ob_start();
// echo "<h1>hoanle123</h1>";
?>
<!-- <h1>hoanle</h1> -->
<?php 
// echo apply_filters('hoanle_filter', ob_get_clean());
// add_filter( 'hoanle_filter', function($output) {
// 	$output = 'hehe123';
// 	return $output;
// });
// $content2 = ob_get_clean();
// ob_clean();
// ob_end_flush();
// echo $content2; 
// exit;
?>








