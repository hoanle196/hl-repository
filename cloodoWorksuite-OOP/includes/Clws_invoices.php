<?php
if ( !function_exists( 'add_action' ) ) {
    echo 'Hi there!  I\'m just a plugin, not much I can do when called directly.';
    exit;
}
class Clws_invoices extends Clws_API {
    public function clws_access_invoices() {
        $this-> clws_invoices_sync();
        Clws_resource::view('invoices.php');
    }
    public function clws_invoices_sync() {
        $res = Clws_invoices::call_api_get(CLWS_API_GET_INVOICES);
        if (is_wp_error($res)) {
            $_SESSION['error'] = $res->get_error_message();
        } elseif ($res['response']['code'] != 200) {                   
            $_SESSION['error'] = 'Client sync error!';                    
        } else {
            $arr = Clws_invoices::swap_json($res['body']);
            $totalSum = sanitize_text_field($arr['meta']['paging']['total']);
            // if ($totalSum == 1) {
            //     echo 'async data';
            // } else {
                $res_all = Clws_invoices::call_api_get( CLWS_API_GET_ALL_INVOICES.$totalSum );
                $all_data = Clws_client::swap_json( $res_all['body']);
                echo '<pre>';
                    print_r($all_data);
                    echo '</pre>';
                    exit;
                $res_data_user = Clws_invoices::call_api_get( CLWS_API_USER_DATA_URL );
                $user_data = Clws_client::swap_json( $res_data_user['body'] )['data']['data'];
                $orders = wc_get_orders([
                    'limit'=> -1
                ]);
                $customArr = [];
                $list_items = [];
                $sub_total = 0;
                $total = 0;
                foreach( $orders as $clwsvalue ) {
                    //get array data oder
                    $data_oder = $clwsvalue->get_data();
                    echo '<pre>';
                    print_r($data_oder);
                    echo '</pre>';
                    //get aray data product in oder
                    foreach( $clwsvalue->get_data()['line_items'] as $value ){
                        $data_item = $value->get_data();
                        $sub_total += $data_item['subtotal'];
                        $total += $data_item['total'];
                        $list_items[] = $data_item;
                    }
                    //get DETAILS obj product by product_id->get data array
                    // $product = wc_get_product( $data_item['product_id'] )->get_data();
                    $customArr[] = [
                        'data_oder' => $data_oder,
                        'data_product' => $list_items,
                        'quantity_product' => count($list_items),
                        'total' => $total,
                        'sub_total' => $sub_total,
                    ];
                }
                foreach( $customArr as $value ) {
                    echo '<pre>';
                    print_r($value);
                    echo '</pre>';
                    $discout = ($value['sub_total'] - $value['total']) * 100 /$value['sub_total'];
                    $obj = $value['data_oder']['date_created'];
                    $date = date("Y-m-d", $obj->getOffsetTimestamp());
                    $data = [
                        'invoice_number' => $value['data_oder']['id'],
                        'issue_date' => $date ,
                        'due_date' => $date,
                        'sub_total' => $value['sub_total'],
                        'total' => $value['total'],
                        'discount' => $discout,
                        'note' => $value['data_oder']['customer_note'],
                        'client_id' => $user_data['id'],
                        'currency_id' => '366',
                        'invoice_items' => [
                        ]
                    ];
                    // $res = Clws_invoices::call_api_post(CLWS_API_POST_INVOICES,$data);
                    echo '<pre>';
                    print_r($res['response']['code']);
                    echo '</pre>';
                    // exit;
                }
            // }
            
        }
        
    }
}