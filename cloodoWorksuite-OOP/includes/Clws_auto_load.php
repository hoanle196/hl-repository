<?php
    if ( !function_exists( 'add_action' ) ) {
        echo 'Hi there!  I\'m just a plugin, not much I can do when called directly.';
        exit;
    }

    spl_autoload_register( function( $class_name ) {
        $path = 'includes/';
        $extension = '.php';
        $arr = explode('\\', $class_name);
        $class = end($arr);
    // echo $class_name;
    // echo "\n";
    // print_r($arr);
    // echo $class;
    // exit;
        $class_path = CLWS_PLUGIN_DIR. $path. $class. $extension ;
        $space_name = CLWS_PLUGIN_DIR. $path. $class_name. $extension ;
    // echo $class_path;
    // echo "\n";
    // echo $space_name;
    // exit;
        if (file_exists($class_path)) {
            require_once(str_replace('\\','/',$class_path));
        } else {
            if (file_exists($space_name)){
                require_once(str_replace('\\','/',$space_name));
            }
        }
    });