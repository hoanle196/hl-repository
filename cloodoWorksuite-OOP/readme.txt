=== WP Worksuite - CRM, Live Chat, Clients, Leads, Project Manager & Notice Board ===
Contributors: cloodoteam
Donate link: https://cloodo.com/
Tags: crm, live chat, clients, leads, project manager, notice board
Requires at least: 4.7
Tested up to: 6.0
Stable tag: 2.0.0
Requires PHP: 7.0
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

WP Worksuite helps you to manage lead database, lead category, lead sources, company projects, your client support in a single system.

== Description ==

Get a unique interactive experience of managing your business independently with the most reliable Wordpress Worksuite  with all the tools and integrations you need for lead management, client and sales activities,ticket support, project management, human resources, financial report and customer service. 

WP Worksuite has all the important features you need to manage your business. It is especially good for agency startup, from small to large scale business, store management or any other kind of interprise organization

The WP Worksuite plugin is so beginner-friendly that all you need is a one-click activation to get started! You will be automatically created a Worksuite account on Cloodo platform to start manage your business.

## WP Worksuite CRM

WP Worksuite gives you ultimate visibility into customer interactions across every team in your organization. That means that everyone will be on the same page from marketing to sales to customer service and operation or client deployment project team, all able to help your customers more effectively. In fact this is a universial workspace for any clients’s orientation ogranization.

Lead Management helps you to manage lead database, lead category, lead sources. Lead is any type of contact which is not yet made any kind of purchase.

- Add unlimited lead with lead information like name, email, mobile, country, website, company
- Assign the company sales agent who will have to take the the lead follow up
- Create custom lead category, add unlimietd any lead custom information for your own organization
- Add unlimited lead campaign to manage and plan your lead development projects of your company
- Add unlimited lead follow up activities which is assign to sales agent or any other member inside your company.

Client Management helps to manage client who did made a purchase even if it is just a pending order.

- Add unlimited client with client information like name, email, mobile, country, website, company
- Assign the company sales agent who will have to take care client customer care of follow up for any cross selling, post selling.
- Create custom client category, add unlimited any client custom information for your own organization with our advanced custome field in each account setting
- Add unlimited client’s project to manage and plan the development projects of your company. 
- Add unlimited client follow up activities which is assign to sales agent or any other member inside your company.
- Create unlimited proposal which can be sent to client to accept. The proposal can be visiple via web browser or download pdf and client can sign their digital signature. 
- Proposal then can be converted to officially contract and linked with invoicing module and project milestone to centralize the financial relationship of your clients.

## WP Worksuite Team Work and Project Management:

Manage your company projects, your client support  in a single system, resulting in empowered teams, satisfied clients, and increased profitability.

- Keep a track of all your projects in most simple way.
- Assign tasks to project members and track the status.
- Add members to your projects and keep them in sync with the progress.
- Using Task Kanban board and filter project task with different criteria
- Creat unlimited Project Milestone with deadline and Cost
- Add unlimited project Documentation, Files, Comment to store and share bewteen project member and client.
- Create various custom field for your own project to suite with your team/company operation

## WP Worksuite Communication:

Notice: allow to post company annoucement to company member or client
Messenger: allow to chat between company member or with client

## Custom API Integration

We are adding more and more feature for CRM which is directly developed into the core worksuite modules or via API plugin integration. The world now is API connection and in fact we are open for custom development request:

- Develop any lead, client, crm integration api plugin for your business
- Provide custom lead import services as per requested
- Develop any task, project management UI for your business

Please do not hesitate to contact us via <support@cloodo.com>

## Coming Soon

We soon to relase core module for ticket support, hrm, finance… and other advanced setting. Actually the update is done weekly.


== Frequently Asked Questions ==

= Is API required to run this plugin? =

Yes. Plugin will register an account automatic after you click register button on first screen on erp.cloodo.com. All your data will be saved here.

== Screenshots ==

1. Dashboard
1. Add Project
1. List Projects
1. Add Lead
1. List Leads
1. Add Client
1. List Clients
1. Add Notice
1. Notice Board
1. Messages Menu
1. Chat Logs

== Changelog ==

= 2.0.0 =
* Update New UI
* Add new feature Projects, Leads, Notice Board, Messages

= 1.1.0 =
* Add Client feature

= 1.0.0 =
* Init

== Upgrade Notice ==

= 1.0 =
* First version