<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Employee;

class EmployeesController extends Controller
{
    public function index(Request $request)
    {
        $employees = Employee::all();
        $item = 5;
        $employees = Employee::paginate($item);
        $pageSum = $employees->lastPage();
        $pageNum = $employees->currentPage();
        $around = 2;
        $next = $pageNum + $around;
        if ($next > $pageSum) {
                $next = $pageSum;
        }
        $pre = $pageNum - $around;
        if ($pre <= 1) $pre = 1;
        $ofset = (request()->input('page', 1) -1 ) * $item;
        return view('Employees.index', [
                "employees" => $employees ?? '',
                "ofset" => $ofset ?? '',
                "next"=> $next ?? '',
                "pre"=> $pre ?? '',
        ]);
    }
    public function create(Request $request)
    {
        return view('employees.create');
    }
    public function store(Request $request, Employee $employees)
    {
        $employees->employee_name = $request->input('employee_name');
        $employees->birthday = $request->input('birthday');
        $employees->gender = $request->input('Gender');
        $employees->salary = $request->input('salary');
        // $query = new Employees();
        // $query->create($res);
        // $employees
        // Employees::create(unset($request->all()['_token']));

        return view('employees.index');
    }
    public function edit(Request $request)
    {
        return view('employees.index');
    }
    public function update(Request $request)
    {
        return view('employees.index');
    }
    public function destroy(Request $request)
    {
        return view('employees.index');
    }
}
