<?php

namespace App\Http\Controllers\Admin\users;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class loginController extends Controller
{
    //
    public function index() {
        return view('Admin.users.login', ['title' => 'login']);
    }
    public function create(Request $request) {
        return view('Admin.users.register', ['title' => 'Register']);
    }
    public function register(Request $request) {
        // dd($request->all());
        $request->validate([
            'name' => 'required',
            'email' => 'email|required',
            'password' => 'required|min:3|max:20',
        ]);
        DB::table('users')->insert([
            'name'=> $request->input('name'),
            'email'=> $request->input('email'),
            'password'=> bcrypt($request->input('password')),
        ]);
        session(['status' => [
            'message' => 'Register successfuly',
            'class' => 'success',
        ]]);
        return redirect('login');
    }
    public function store(Request $request) {
        // dd($request->all());
        $request->validate([
            'email' => 'email|required',
            'password' => 'required|min:3|max:20',
        ]);
        if (
            Auth::attempt([
                'email' => $request->email,
                'password' => $request->password,
            ], $request->remember)
            ) {
            session(['status' => [
                'message' => 'Login success',
                'class' => 'success',
                
            ],'login'=> true, ]);
            return redirect()->route('employee')->with('title', 'abc');
        } else {
            session(['status' => [
                'message' => 'Login fails',
                'class' => 'danger',
            ]]);
            return redirect()->back();
        }
    }
    public function destroy(Request $request) {
        if ($request->session()->has('login')) {
            session(['status' => [
                'message' => 'logout successfuly',
                'class' => 'success',
            ]]);
            session()->forget('login');
        }
        return redirect()->route('login');
    }
}
