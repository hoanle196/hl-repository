
@extends('layouts.layoutLogin.main')
@section ('content')
  <body class="hold-transition login-page">
  <div class="register-box">
    <div class="register-logo">
      <a href="#"><b>Register</b></a>
    </div>
    <!-- /.login-logo -->
    <div class="card">
        <div class="card-body register-card-body">
          <p class="login-box-msg">Register a new membership</p>
    
          <form action="{{ route('register') }}" method="post">
            @csrf
            <em class="badge text-danger">
                @error('name')
                    {{ $message }}
                @enderror
              </em>
            <div class="input-group mb-3">
              <input type="text" name="name"  value="{{ old('name') }}" class="form-control" placeholder="Full name">
              <div class="input-group-append">
                <div class="input-group-text">
                  <span class="fas fa-user"></span>
                </div>
              </div>
            </div>
            <em class="badge text-danger">
                @error('email')
                    {{ $message }}
                @enderror
              </em>
            <div class="input-group mb-3">
              <input type="email" name="email" value="{{ old('email') }}" class="form-control" placeholder="Email">
              <div class="input-group-append">
                <div class="input-group-text">
                  <span class="fas fa-envelope"></span>
                </div>
              </div>
            </div>
            <em class="badge text-danger">
                @error('password')
                    {{ $message }}
                @enderror
              </em>
            <div class="input-group mb-3">
              <input type="password" name="password" class="form-control" placeholder="Password">
              <div class="input-group-append">
                <div class="input-group-text">
                  <span class="fas fa-lock"></span>
                </div>
              </div>
            </div>
            <div class="row">
              <!-- /.col -->
              <div class="col-4">
                <a class="btn btn-secondary btn-block" href="{{ route('login')}}">Back</a>
              </div>
              <div class="col-8">
                <button type="submit" class="btn btn-primary btn-block">Register</button>
              </div>
              <!-- /.col -->
            </div>
          </form>
        </div>
        <!-- /.form-box -->
      </div><!-- /.card -->
  </div>
@endsection









