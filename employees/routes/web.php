<?php
session_start();
use App\Http\Controllers\Admin\users\loginController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\EmployeesController;
use App\Http\Controllers\DepartmentsController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('login', [loginController::class, 'index'])->name('login');
Route::get('logout', [loginController::class, 'destroy'])->name('logout');
Route::get('register', [loginController::class, 'create'])->name('register');
Route::post('register', [loginController::class, 'register'])->name('register');
Route::post('login', [loginController::class, 'store'])->name('store');
Route::middleware(['CustomMiddle'])->group(function () {
    Route::get('employee', [EmployeesController::class, 'index'])->name('employee');
    Route::get('/create', [EmployeesController::class, 'create'])->name('create_employee');
    Route::post('/create', [EmployeesController::class, 'store'])->name('store_employee');
    Route::get('/edit/{id}', [EmployeesController::class, 'edit'])->name('edit_employee');
    Route::put('/edit/{id}', [EmployeesController::class, 'update'])->name('update_employee');
    Route::get('/destroy/{id}', [EmployeesController::class, 'destroy'])->name('destroy_employee');




    Route::get('department', [DepartmentsController::class, 'index'])->name('department');

});