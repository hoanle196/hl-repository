<?php 
    if ( !function_exists( 'add_action' ) ) {
        echo 'Hi there!  I\'m just a plugin, not much I can do when called directly.';
        exit;
    }
class HL_Weather_Api {
    public static $url = 'https://erp.cloodo.com/api/v1/product/?fields=id,name,price,description,taxes,allow_purchase,category,hsn_sac_code&offset=0';
    public static $method = 'GET';
    public static function get_json($json) {
        return json_decode($json, true);
    }
    public static function Run_API_Get() {
        $arrs = [
            'method' => static::$method,
            'timeout' => 10,
            'redirection' => 5,
            'blocking' => true,
            'cookie' => [],
            'headers' => [
                'X-requested-Width'=> 'XMLHttpRequest',
                'Authorization'=> 'Bearer '.sanitize_text_field(get_option('cloodo_token'))
            ],
            'body' => [
            ]
        ];
        $res = wp_remote_request(static::$url, $arrs);
        if (is_wp_error($res)) {
            $_SESSION['error'] =  $res->get_error_message();
        } elseif ($res['response']['code'] != 200) {                   
            $_SESSION['error'] = 'view lead error!';                    
        } else {
            $arr = self::get_json($res['body']);
        }
        if ($arr) {
            return $arr;
        }
    }
    public static function Run_API_Post($info, $data) {
        $arrs = [
            'method' => $info['method'],
            'timeout' => 10,
            'redirection' => 10,
            'blocking' => true,
            'cookie' => [],
            'headers' => [
                'X-requested-Width'=>'XMLHttpRequest',
                'Authorization'=>'Bearer '.sanitize_text_field(get_option('cloodo_token'))
            ],
            'body' => [
                'name' => $data['name'] ,
                'price' => $data['price'],
                'hsn_sac_code' => $data['id'],
                'description' => $data['short_description'],
            ]
        ];
        $res = wp_remote_request($info['url'], $arrs);
        return $res;
        if (is_wp_error($res)) {
            $_SESSION['error'] =  $res->get_error_message();
        } elseif ($res['response']['code'] != 200) {                   
            $_SESSION['error'] = 'view lead error!';                    
        } else {
            $arr = self::get_json($res['body']);
        }
        if ($arr) {
            return $arr;
        }
    }
    public static function hlw_get_weather($data=[]) {
        if ($data) {
            $res =[];
            foreach ($data as $city_name) {
                $url = "https://api.openweathermap.org/data/2.5/weather?q={$city_name}&APPID=".HL_WEATHER_API_KEY ;
                @$fget = file_get_contents($url);
                if ($fget) {
                    $res[] = self::get_json($fget);
                }
            }
            if ($res) return $res;
        }
        return false;
    }
}
// class Dasboar {
//     public function __construct() {
        
//     }
//     public function index() {
//         // khi vào menu thì chạy funct này
//     }
//     public function click() {
//         // khi click zô nút code xư lý
//         if(isset ($_POST['checkClick'])) {

//         }
//     }
// }
// ///////////////// index page////////////
// $a = new Dasboar;
// $a -> click();