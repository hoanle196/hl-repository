<?php 
    if ( !function_exists( 'add_action' ) ) {
        echo 'Hi there!  I\'m just a plugin, not much I can do when called directly.';
        exit;
    }
class HL_Weather {
    public $emailadm;
    public $id;
    public $user;
    public $namesite;
    public $user_login;
    public $user_email;
    public $company_name;
    public function __construct() {
        $this->emailadm = sanitize_text_field(get_option( 'admin_email'));
        $this->id = get_current_user_id();
        $this->user = get_userdata($this->id);
        $this->namesite = get_bloginfo();
        $this->user_login = sanitize_text_field($this->user->user_login);
        $this->user_email = sanitize_email($this->user->user_email);
        $this->company_name = (explode('.', $this->namesite))[0];
        add_action('admin_menu', function() {
            add_menu_page(
                'dashboard-test', // title menu
                'Worksuite-test', // name menu
                'manage_options',// area supper admin and admin 
                'Test', // Slug menu
                [$this,'demo'], // display function 
                'dashicons-businessman', // icon menu
                '8'
            );
            add_submenu_page(
                'Test', // Slug menu parent
                'noticeaa', // title page
                'Notice', // name menu
                'manage_options', // area supper admin and admin
                'noticea', // Slug menu
                [$this,'note'], // display function
            );
            add_submenu_page(
                'Test',
                'Product Filter By Ambient Noise',
                'Product',
                'manage_options',
                'taxonomy=product-filter-ambient-noise',
                [$this,'abc'],
            );
        });
    }
    public function abc() {
        echo '123';
        $abc = new Post_API; 
        echo '<pre>';
		print_r($abc->Run_Request_Post()) ; 
		echo '</pre>';
    }
    public function note() {
        require_once(str_replace('\\','/',HL_WEATHER_PLUGIN_DIR.'views/viewsdemo.php'));
    }
    public function demo() {
        require_once(str_replace('\\','/',HL_WEATHER_PLUGIN_DIR.'views/setting.php'));
    }
    public function Register() {
        if (isset($_POST['Register_quickly'])) {
            $pw = substr(md5(rand(0, 99999)), 0, 6);
            $arrs =[
                'method'=> 'POST',
                'body'=>[
                'company_name'=> $this->company_name,
                'email'=> $this->emailadm,
                'website' => $this->namesite,
                'password'=> $pw,
                'password_confirmation'=> $pw
                ],
                'timeout'=> 10,
                'redirection'=> 5,
                'blocking'=> true,
                'headers'=> [],
                'cookie'=> [],
            ];
            $res = wp_remote_request('https://erp.cloodo.com/api/v1/create-user',$arrs);
            if ( is_wp_error( $res ) ) {
                $_SESSION['error'] = $res->get_error_message();
            } else {
                $result = isset($res['body']) ? json_decode($res['body'], true) : 0;
                if(isset($result['status']) == 'success') {
                    ///////////////////////////////// register and login get token !
                    $arrs = [
                        'method'=> 'POST',
                        'body'=>['email'=>$this->emailadm,'password'=> $pw],
                        'timeout'=>100,
                        'redirection'=>5,
                        'blocking'=>true,
                        'headers'=>[],
                        'cookie'=>[],
                    ];
                    $res = wp_remote_request('https://erp.cloodo.com/api/v1/auth/login', $arrs);
                    if (isset($res['response']['code']) != 200) {
                        $_SESSION['error'] = $res['response']['code'].' '.$res['response']['message'];
                    } else {
                        $to = $this->emailadm;
                        $subject ='Thư Cám ơn và gửi Mật Khẩu cho bạn !';
                        $message =  "Chào bạn <b>{$this->user_login}</b><br> Mật khẩu của bạn là : {$pw}";
                        $headers = 'From:hoanle161996@gmail.com' . "\r\n" .
                        'Reply-To:hoanle161996@gmail.com' . "\r\n";
                        $sent = wp_mail($to, $subject, strip_tags($message), $headers);
                        $res = HL_Weather_Api::get_json($res['body']);
                        // $res = json_decode($res['body'], true);
                        $id_token = $res['data']['token'];
                        $_SESSION['token'] = $id_token;
                        update_option('cloodo_token', $id_token);
                        $dataoption[] = [
                            "token"=> $id_token,
                            "email"=> $this->emailadm,
                        ];
                        $dataoption = maybe_serialize($dataoption);
                        update_option('clws_info', $dataoption);
                        // echo "
                        //     <script>
                        //         setTimeout(window.onload = function() {
                        //             jQuery(document).find( '#login' ).remove();
                        //             var myIfr = window.frames['iframeclws'].contentWindow;
                        //             var val = myIfr.postMessage('".get_option('cloodo_token')."','".esc_url(CLWS_IFRAME_URL)."check-login');
                        //         },3000)
                        //     </script>";
                            // require(str_replace('\\','/',HL_WEATHER_PLUGIN_DIR.'views/setting.php'));
                            // return;
                    }
                } else {
                    $_SESSION['error'] = 'The Accounts already exists or has not activated email, please try again !';
                }
            }
        }
    }
    
}