<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Departments;

class DepartmentsController extends Controller
{
    //
    public function index(Request $request)
    {
        $departments = Departments::all();
        $item = 5;
        $departments = Departments::paginate($item);
        $pageSum = $departments->lastPage();
        $pageNum = $departments->currentPage();
        $around = 2;
        $next = $pageNum + $around;
        if ($next > $pageSum) {
                $next = $pageSum;
        }
        $pre = $pageNum - $around;
        if ($pre <= 1) $pre = 1;
        $ofset = (request()->input('page', 1) -1 ) * $item;
        return view('departments.index', [
                "departments" => $departments ?? '',
                "ofset" => $ofset ?? '',
                "next"=> $next ?? '',
                "pre"=> $pre ?? '',
        ]);
    }
    public function create(Request $request)
    {
        return view('departments.create');
    }
    public function store(Request $request, Departments $departments)
    {
        
        $departments->department_name = $request->input('department_name');
        $departments->description = $request->input('description');
        $departments->save();
        return redirect('department');
    }
    public function edit(Request $request)
    {
        return view('departments.index');
    }
    public function update(Request $request)
    {
        return view('departments.index');
    }
    public function destroy(Request $request)
    {
        return view('departments.index');
    }
}
