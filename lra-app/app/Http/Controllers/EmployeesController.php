<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Employees;
use App\Models\Departments;

class EmployeesController extends Controller
{
    public function index(Request $request)
    {
        return view('employees.index');
    }
    public function create(Request $request)
    {
        return view('employees.create');
    }
    public function store(Request $request, Employees $employees)
    {
        $employees->employee_name = $request->input('employee_name');
        $employees->birthday = $request->input('birthday');
        $employees->gender = $request->input('Gender');
        $employees->salary = $request->input('salary');
        // $query = new Employees();
        // $query->create($res);
        // $employees
        // Employees::create(unset($request->all()['_token']));

        return view('employees.index');
    }
    public function edit(Request $request)
    {
        return view('employees.index');
    }
    public function update(Request $request)
    {
        return view('employees.index');
    }
    public function destroy(Request $request)
    {
        return view('employees.index');
    }
}
