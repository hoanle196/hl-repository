<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Controller;
use App\Http\Controllers\EmployeesController;
use App\Http\Controllers\DepartmentsController;
use App\Http\Controllers\Auth\AuthenticationController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [Controller::class,'index']);
Route::get('/login', [Controller::class,'login'])->name('login');
//route employee
Route::get('/employee', [EmployeesController::class,'index'])->name('employee');
Route::prefix('employee')->group(function () {
    Route::get('/create', [EmployeesController::class, 'create'])->name('create_employee');
    Route::post('/create', [EmployeesController::class, 'store'])->name('store_employee');
    Route::get('/edit/{id}', [EmployeesController::class, 'edit'])->name('edit_employee');
    Route::put('/edit/{id}', [EmployeesController::class, 'update'])->name('update_employee');
    Route::get('/destroy/{id}', [EmployeesController::class, 'destroy'])->name('destroy_employee');
    
});
// route department
Route::get('/department', [DepartmentsController::class,'index'])->name('department');
Route::prefix('department')->group(function () {
    Route::get('/create', [DepartmentsController::class, 'create'])->name('create_department');
    Route::post('/create', [DepartmentsController::class, 'store'])->name('store_department');
    Route::get('/edit/{id}', [DepartmentsController::class, 'edit'])->name('edit_department');
    Route::put('/edit/{id}', [DepartmentsController::class, 'update'])->name('update_department');
    Route::get('/destroy/{id}', [DepartmentsController::class, 'destroy'])->name('destroy_department');
    
});
// route regiser department

Route::post('/register', [AuthenticationController::class, 'register'])->name('register');
Route::post('/sign-in', [AuthenticationController::class, 'signin'])->name('sign-in');
Route::get('/forgotPass', [AuthenticationController::class, 'forgotPass'])->name('forgotPass');