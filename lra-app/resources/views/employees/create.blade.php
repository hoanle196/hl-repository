@extends('layouts.main')
@section ('content')
@include('layouts.message')
<div class="container">
    <div class="result">
        <h1> Create Employee </h1>
        <form action=" {{ route('store_employee') }} " method="POST">
            @csrf
            <label for="employee_name">Employee Name</label>
            <input type="text" id="employee_name" name="employee_name" placeholder="Employee name..">
        
            <label for="birthday">birthday</label>
            <input type="text" id="birthday" name="birthday" placeholder="Birthday..">

            <label for="Gender">Gender</label>
            <select id="Gender" name="Gender">
                <option value="#"> Pick Gender </option>
                <option value="male">male</option>
                <option value="female">female</option>
            </select>

            <label for="salary">Salary</label>
            <input type="number" min="0"  max="10000" id="salary" name="salary" placeholder="Salary..">
        
            <input type="submit" value="Submit">
        </form>
    </div>
</div>
@endsection