@extends('layouts.main')
@section ('content')
@include('layouts.message')
<div class="container">
    <div class="result">
        <a href="{{route('create_department')}}"><button type="button" class="btn btn-primary">add new Department</button></a>
        <table class="table">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">department_name</th>
                    <th scope="col">description</th>
                    <th scope="col">action</th>
                </tr>
            </thead>
            <tbody>
            
                @foreach ( $departments as $index=>$department ) 
                    <tr>
                        <th scope="row">{{++$ofset}}</th>
                        <td><a href="">{{$department->department_name}}</a></td>
                        <td>{{$department->description}}</td>
                        <td>
                            <a class="btn btn-info" href="{{ route('edit_department', $department->id) }}">edit</a>
                            <button type="button" class="btn btn-danger" data-url ="{{ route('destroy_department', $department->id) }}" data-bs-toggle="modal" data-bs-target="#exampleModal">delete</button>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        {{-- {{ $departments->links()}} --}}
    </div>
    {{ $departments->links('layouts.customPaginate', compact('ofset', 'next', 'pre'))}}
</div>
@endsection
