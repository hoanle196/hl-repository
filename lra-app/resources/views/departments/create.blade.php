@extends('layouts.main')
@section ('content')
@include('layouts.message')
<div class="container">
    <div class="result">
        <h1> Create Department </h1>
        <form action=" {{ route('store_department') }} " method="POST">
            @csrf
            <label for="department_name">department_name</label>
            <input type="text" id="department_name" name="department_name" placeholder="department_name..">
        
            <label for="description">description</label>
            <input type="text" id="description" name="description" placeholder="description..">
            
            <input type="submit" value="Submit">
        </form>
    </div>
</div>
@endsection