<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="	https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{ URL::asset('css/app.css') }}">
    <script defer src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.3/jquery.min.js" integrity="sha512-STof4xm1wgkfm7heWqFJVn58Hm3EtS31XFaagaa8VMReCXAkQnJZ+jEy8PCC/iT18dFy95WcExNHFTqLyp72eQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script defer src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js"></script>
    <script defer type="text/javascript" src="{{ URL::asset('js/script.js') }}"></script>
    <title>Document</title>
</head>
<header>
    @if (!(request()->is('login')))
    <nav>
        <a href="{{ route('employee') }}">Employee</a>
        <a href="{{ route('department') }}">Department</a>
        {{-- <a href="{{ route('registerDe') }}">register Department</a> --}}
    </nav>
    @endif
</header>
<body>
    {{-- <header style="background: rgb(161, 135, 135);height:100px" >
        <ul class="nav nav-pills">
            <li class="nav-item">
                <a 
                    class="nav-link {{ request()->is('/') ? 'active' : '' }}" 
                    href="{{ route('index') }}">Home
                </a>
            </li>
            <li class="nav-item">
                <a 
                    class="nav-link {{ request()->is('product') ? 'active' : '' }}" 
                    href="{{ route('product') }}">Product
                </a>
            </li>
            <li class="nav-item">
                <a 
                    class="nav-link {{ request()->is('post') ? 'active' : '' }}" 
                    href="{{ route('post') }}">post
                </a>
            </li>
            <li class="nav-item">
                <a 
                    class="nav-link {{ request()->is('food') ? 'active' : '' }}" 
                    href="{{ route('food') }}" 
                    tabindex="-1" 
                    aria-disabled="true">food
                </a>
            </li>
        </ul>
    </header> --}}